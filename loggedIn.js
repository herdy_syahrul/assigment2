import React, {Component} from 'react'
import {View, StyleSheet, Button, TextInput, Text, Image, TouchableOpacity} from 'react-native'
import Bread from '../assets/bread.png'
import Icon from 'react-native-vector-icons/FontAwesome5';

class Loggedin extends Component {
  constructor(){
        super()
        this.state = {
            name:true,
            icontype:'eye-slash',
            RsPass:'',
            Username:'',
            UserPass:'',
            iconCo: 'orange'
        }
    }

  alerte = () => {
    const UserN = this.state.Username
    const UserP = this.state.RsPass
    if(UserN != '' && UserP != ''){
      alert('anda berhasil login')
      this.props.navigation.navigate("Beranda", {
        data: this.state.Username,
      })
    } else {
      alert('anda harus mengisi semua kolom input')
    }
  }

  show = () => {
      this.name = !this.name
      this.setState({ name: false });
      this.setState({ icontype: 'eye' });
      if (this.name === false) {
        this.setState({name:true})
        this.setState({icontype:'eye-slash'})
      }
  }

  mulai = () => {
    this.setState({RsPass:''})
  }

  render(){
   return (
     <View style={styles.container}>
        <Image source={Bread}/>
        <Text style={styles.text}>Hesya's Cake</Text>
        <Text style={{color:'grey', marginVertical:20}}>Please Login make a recipe</Text>
        
        <View style={styles.inp}>
          <Icon name="user-lock" size={25} style={{marginRight:5, color:'orange'}} />
          <TextInput placeholder="Username/Email" onChangeText={(text) => this.setState({Username:text})} style={{textAlign:'left', marginLeft:25}}></TextInput>
        </View>
         <View style={styles.inp}>
            <Icon name="key" size={25} style={{marginRight:5, color:'orange', flex:1}} />
            <TextInput secureTextEntry={this.state.name} placeholder="Password" value={this.state.RsPass} onChangeText={(text) => this.setState({RsPass:text})} style={{textAlign:'left', flex:3}}></TextInput>
            <TouchableOpacity onPress={() => this.show()} style={{marginRight:5, color:'orange',alignItems:'flex-end'}}>
              <Icon name={this.state.icontype} size={25} />
          </TouchableOpacity>
         </View>
        
        


        {/* <TextInput secureTextEntry placeholder="Password"  style={styles.inp}></TextInput> */}

          <TouchableOpacity onPress={() => this.alerte()} style={styles.button}>
            <Text style={{fontWeight:'bold', color:'white', fontSize:15}}>Login</Text>
          </TouchableOpacity>

          <View style={{ flexDirection:'column', textAlign:'left', justifyContent:'center', height:150, width:'70%'}}>
           <View style={{ flexDirection:'row',  marginBottom:10,}}>
             <Text>Forgot Password ? </Text>
             <TouchableOpacity  onPress={() => this.mulai()}>
                <Text style={{color:'blue'}}>Reset password ?</Text>
             </TouchableOpacity>
           </View>
            <View style={{ flexDirection:'row'}}>
                <Text>Dont have an acount ? </Text>
             <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')}>
                <Text style={{color:'blue'}}>Sign Up ?</Text>
             </TouchableOpacity>
           </View>
          </View>
     </View>
   )
  }
}

export default Loggedin

const styles = StyleSheet.create({
   container: {
    flex:1,
    backgroundColor:'white',
    justifyContent:'center',
    alignItems:'center',
    fontFamily:'poppins'
  },
  text: {
    color:'salmon',
    fontSize: 30,
    fontWeight:'100',
    fontFamily:'verdana'
  },
  inp: {
    borderBottomColor: "lightgrey",
    borderBottomWidth: 1,
    margin: 5,
    paddingHorizontal:10,
    width:'80%',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'flex-start'
  },
    button: {
    alignItems: "center",
    backgroundColor: "#F45C43",
    padding: 13,
    width:'70%',
    marginTop:20,
    borderRadius:50,
    elevation: 5
  },
})