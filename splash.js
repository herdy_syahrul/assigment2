import React, { Component } from 'react'
import { Text, View, StyleSheet, ImageBackground, Image } from 'react-native'
import Bread from '../../assets/bread.png'
import {StackActions} from '@react-navigation/native'

export class Splash extends Component {
    constructor(props){
        super(props)
        setTimeout(() => {
            this.props.navigation.replace("logged")
        }, 3000);
    }
    render() {
        return (
            // <ImageBackground source={require('../../assets/gm.jpg')} style={styles.container}>
            //     <View style={styles.inner}>
            //        <Text style={{fontSize:30, color:'white', fontWeight:'bold'}}>Hesyaa-App</Text>
            //     </View>
            // </ImageBackground>
            <View style={styles.container}>
              <Image source={Bread} height="200" width="300" />
              <Text style={{fontSize:40, color:'#FDF6F0', fontWeight:'bold'}}>Hesya's Cake</Text>
              <Text style={{fontSize:20,color:'#FDF6F0',}}>Share your recipes to everyone</Text>
            </View>
        )
    }
}

export default Splash

const styles = StyleSheet.create({
    container: {
     flex:1,
     alignItems:'center',
     justifyContent:'center',
     backgroundColor:'#E98580'
    },
    inner: {
        width:'80%',
        height:'80%',
        backgroundColor: 'rgba(48,48,38, .5)',
        flex:1, alignItems:'center', justifyContent:'center'
    }
})
