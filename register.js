import React, {Component} from 'react'
import {View, Text, StyleSheet, Image, TextInput, TouchableOpacity} from 'react-native'
import Bread from '../assets/bread.png'
import Icon from 'react-native-vector-icons/FontAwesome5';

class Register extends Component {
    render(){
     return (
      <View style={styles.container}>
          {/* <Image source={Bread} style={{}}/> */}
              <Text style={styles.text}>Let's Create Account</Text>
              {/* <Text style={{marginBottom:5}}>Please register with valid data</Text> */}

          <View style={styles.inp2}>
            <Icon name="user-circle" size={20} style={{marginRight:5, color:'salmon'}} />
            <TextInput placeholder="FullName" style={{paddingLeft:20, marginLeft:28}}></TextInput>
          </View>
          <View style={styles.inp2}>
             <Icon name="user-circle" size={20} style={{marginRight:5, color:'salmon'}} />
             <TextInput placeholder="Username" style={{paddingLeft:20, marginLeft:28}}></TextInput>
          </View>
          <View style={styles.inp2}>
             <Icon name="envelope-open" size={20} style={{marginRight:5, color:'salmon'}} />
             <TextInput secureTextEntry placeholder="Email" style={{paddingLeft:28, marginLeft:20}}></TextInput>
          </View>
          <View style={styles.inp}>
             <Icon name="key" size={20} style={{marginRight:5, flex:1, color:'salmon'}} />
             <TextInput secureTextEntry placeholder="Password" style={{ flex:3, marginLeft:20}}></TextInput>
             <Icon name="eye-slash" size={20} style={{marginRight:5, color:'salmon', flex:1}} />
          </View>
          <View style={styles.inp}>
            <Icon name="user-check" size={20} style={{flex:1,marginRight:5, color:'salmon'}} />
            <TextInput secureTextEntry placeholder="Confirm Password" style={{flex:3, marginLeft:20}}></TextInput>
            <Icon name="eye-slash" size={20} style={{flex:1,marginRight:5, color:'salmon', flex:1}} />
          </View>
         
          <TouchableOpacity style={styles.btn}>
            <Text style={{color:'white', textAlign:'center', fontWeight:'bold'}}>Register</Text>
          </TouchableOpacity>

        <View style={{ flexDirection:'column', textAlign:'left', justifyContent:'center', height:150, width:'70%'}}>
          <View style={{ flexDirection:'row',  marginBottom:10,}}>
             <Text>Forgot Password ? </Text>
             <Text style={{color:'blue'}}>Reset password ?</Text>
          </View>
          <View style={{ flexDirection:'row'}}>
          <Text>Already have an acount ? </Text>
             <TouchableOpacity onPress={() => this.props.navigation.navigate('logged')}>
                <Text style={{color:'blue'}}>Login ?</Text>
             </TouchableOpacity>
          </View>
        </View>
      </View>
     )
    }
}

export default Register

const styles = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor:'white',
      justifyContent:'center',
      alignItems:'center',
      fontFamily:'poppins',
      backgroundColor:'white'
    },
     text: {
      color:'salmon',
      fontSize: 28,
      fontWeight:'normal',
      fontFamily:'verdana',
      marginVertical:15
    },
    inp: {
      // borderWidth:,
      margin: 5,
      paddingHorizontal:10,
      width:'80%',
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'space-between',
      borderColor:'orange',
      borderWidth: 0.8,
      backgroundColor:'#FCD8D4',
      elevation: 5
  },
   inp2: {
      borderWidth:0.5,
      margin: 5,
      paddingHorizontal:10,
      width:'80%',
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'flex-start',
      borderColor:'orange',
      borderWidth: 0.8,
      backgroundColor:'#FCD8D4',
      elevation: 5
   },
    btn: {
      padding:10,
      marginVertical:20,
      elevation:5,
      width:'80%',
      backgroundColor:'#F45C43'
    }
})