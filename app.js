/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import type { Node } from 'react';
import {
  View,
  Text
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/Screens/HomeScreen';
import Detail from './src/Screens/DetailsScreen/DetailScreen'
import Table from './src/assigment/Student'
import Splash from './src/Screens/SplashScreen/Splash'
import logged from './src/assigment/Loggedin'
import SignUp from './src/assigment/Register'
import Beranda from './src/Screens/HomeScreen/Beranda'

const Stack = createStackNavigator();

const App: () => Node = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Detail" component={Detail} />
        <Stack.Screen name="Table" component={Table} />
        <Stack.Screen name="Beranda" component={Beranda} options={{headerShown:false}}/>
        <Stack.Screen name="logged" component={logged} options={{headerShown:false}}/>
        <Stack.Screen name="SignUp" component={SignUp} options={{headerShown:false}}/>
        <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
